<?php
/*
 * --------------------------------------------------
 * Copyright (c) 2020 - 2021 Alailson Barbosa Ribeiro
 * --------------------------------------------------
 * @ Author Info: ribeiro@alailson.com.br
 * @ Create Time: 2021-04-24 01:21:11
 * @ Modified At: 2021-10-28 19:21:12
 * --------------------------------------------------
 * Author Quote: I love do this ? Enlightened by God!
 * --------------------------------------------------
 */

/* Obijetivo: mudar o sistema de login para sso.spturis.com.br */

/* Replace Wordpress authentication */
add_filter('authenticate', array('SSOAuthentication', 'authenticate'), 30, 3);

/* Wordpress Options */
add_option('sso_create_new', 'true');

/* Suppress password reset */
add_action('lost_password', array('SSOAuthentication', 'disable_password'));
add_action('retrieve_password', array('SSOAuthentication', 'disable_password'));
add_action('password_reset', array('SSOAuthentication', 'disable_password'));
add_filter('show_password_fields', array('SSOAuthentication', 'show_password_fields'));

function mapp_user_disable_admin_bar()
{
    if (current_user_can('administrator') || current_user_can('contributor')) {
        // user can view admin bar
        show_admin_bar(true); // this line isn't essentially needed by default...

        //write_log("[wp-ssoauth] ADMIN");
    } else {
        // hide intens to a guest user.
        show_admin_bar(false);
        //write_log("[wp-ssoauth] GUEST");
    }
}

add_action('after_setup_theme', 'mapp_user_disable_admin_bar');

wp_enqueue_style('mapp-sso-css',  '/wp-content/plugins/mapp/auth/login.css', false, random_int(123, 987), 'all' );
global $wp_styles;
$wp_styles->add_data('mmapp-sso-css', 'title', 'my_stylesheet_title');


function sso_enqueue_script() {
    wp_enqueue_script( 'mapp-sso', '/wp-content/plugins/mapp/auth/login.js', array(), random_int(123, 987), false );
}
add_action( 'login_enqueue_scripts', 'sso_enqueue_script', 1 );

function mapp_enqueue_script() {
    wp_enqueue_script( 'mapp-plugin', '/wp-content/plugins/mapp/load.js', array(), random_int(123, 987), false );
}
add_action( 'wp_head', 'mapp_enqueue_script', 1);

/* Prefill login form with mail */
// As part of WP login form construction, call our function
add_filter('login_form', 'ims_login_form_prefill');

function ims_login_form_prefill()
{
    // Output jQuery to pre-fill the box
    if (isset($_REQUEST['u'])) { // Make sure a username was passed
?>
        <script type="text/javascript">
            var el = document.getElementById("user_login");
            el.value = "<?php echo ($_REQUEST['u']); ?>";

            console.log('teste');

        </script>
        <?php
    }
}

/* Suppress email change */
function ims_script_enqueuer()
{
    if (current_user_can('edit_users') == false) {
        echo '
        <script type="text/javascript">
            jQuery(document).ready( function($) {
                $(".user-email-wrap #email").prop("disabled", true);
            });
        </script>
        ';
    }
}

add_action('admin_head-profile.php', 'ims_script_enqueuer');

/* Logging Function Wrapper */
if (!function_exists('write_log')) {
    function write_log($log)
    {
        if (is_array($log) || is_object($log)) {
            error_log(print_r($log, true));
        } else {
            error_log($log);
        }
    }
}


if (!class_exists('SSOAuthentication')) {
    class SSOAuthentication
    {

        static function authenticate($user = null, $user_name = '', $password = '')
        {

            /* remove existing authentication function */
            remove_action('authenticate', 'wp_authenticate_username_password', 20);

            /* see if user already logged in */
            if (is_a($user, 'WP_User')) {
                return $user;
            }

            /* check non-void arguments */
            if (empty($user_name) || empty($password)) {
                return new WP_Error('invalid_user_name', __('<strong>ATENÇÃO!</strong> Para acesso é obrigatório informar seu email@spturis.com e sua senha de acesso ao sistema de email.'));
            }

            /* authenticate over SSO */
            list($auth_result, $user_email) = SSOAuthentication::sso_authenticate($user_name, $password);
            if (!$auth_result) {
                if (is_a($auth_result, 'WP_Error')) {
                    return new WP_Error('invalid_user_name', __('<strong>ERROR</strong>: ' . $auth_result));
                } else {
                    return new WP_Error('invalid_user_name', __('<strong>ATENÇÃO!</strong> Para acesso é obrigatório informar seu email@spturis.com e sua senha de acesso ao sistema de email.'));
                }
            }

            $user_id = email_exists($user_email);
            if (!$user_id) {

                /* user does not yet exist */
                if (SSOAuthentication::get_opt_create_new()) {

                    /* create new user */
                    $user_id = wp_create_user(SSOAuthentication::get_user_name($user_email), $password, $user_email);
                } else {
                    return new WP_Error('registration_not_allowed', __('<strong>ERROR</strong>: Novos registros np WP desabilidtado.'));
                }
            }

            /* return existing user */
            $user = new WP_User($user_id);
            return $user;
        }

        function sso_authenticate($user_name, $password)
        {
            if ($_SERVER['REQUEST_URI'] !== '/xmlrpc.php') {

                //* INI ADD BY MAPP.COMPANY ------------------------------------------------------------------ *//

                /* Cloud Flare Firewall Rule
wp-protect

(http.request.uri contains "/wp-login.php") or (http.request.uri.path contains "/wp-admin") or (http.request.uri contains "/xmlrpc.php")


IPs AS 177.8.168/22

(ip.geoip.asnum eq 53231)

                */
                /* Try to login over SSO */

                session_start();

                if (!preg_match('#@#', $user_name)) {
                    $user_name .= '@spturis.com';
                }

                $user = $user_name;
                $pass = $password;

                function get_client_ip()
                {
                    $ipaddress = [];
                    if (isset($_SERVER['HTTP_CF_IPCOUNTRY']))
                        $ipaddress[] = $_SERVER['HTTP_CF_IPCOUNTRY'];
                    if (isset($_SERVER['HTTP_CLIENT_IP']))
                        $ipaddress[] = $_SERVER['HTTP_CLIENT_IP'];
                    if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
                        $ipaddress[] = $_SERVER['HTTP_X_FORWARDED_FOR'];
                    if (isset($_SERVER['HTTP_X_FORWARDED']))
                        $ipaddress[] = $_SERVER['HTTP_X_FORWARDED'];
                    if (isset($_SERVER['HTTP_FORWARDED_FOR']))
                        $ipaddress[] = $_SERVER['HTTP_FORWARDED_FOR'];
                    if (isset($_SERVER['HTTP_FORWARDED']))
                        $ipaddress[] = $_SERVER['HTTP_FORWARDED'];
                    if (isset($_SERVER['REMOTE_ADDR']))
                        $ipaddress[] = $_SERVER['REMOTE_ADDR'];
                    if (isset($_SERVER['REMOTE_ADDR']))
                        $ipaddress[] = $_SERVER['REMOTE_ADDR'];
                    if (isset($_SERVER['HTTP_USER_AGENT']))
                        $ipaddress[] = $_SERVER['HTTP_USER_AGENT'];
                    if (isset($_SERVER['HTTP_SEC_CH_UA_PLATFORM']))
                        $ipaddress[] = $_SERVER['HTTP_SEC_CH_UA_PLATFORM'];
                    if (isset($_SERVER['HTTP_SEC_CH_UA_MOBILE']))
                        $ipaddress[] = $_SERVER['HTTP_SEC_CH_UA_MOBILE'];
                    if (isset($_SERVER['REMOTE_ADDR']))
                        $ipaddress[] = $_SERVER['HTTP_SEC_CH_UA'];
                    return array_unique(array_filter($ipaddress, 'strlen'));
                }

                $href = [
                    'url' => (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'],
                    'server' => $_SERVER['SERVER_ADDR'],
                    'path' => $_SERVER['SCRIPT_FILENAME'],
                    'origen' => get_client_ip()
                ];

                $pfix = '0a1A2b3B4c5C6D7d8E9e0F9f8G7g6H5h4I3i2J1j01K2k3L4l5M6m7N8n9O0o9P8p7Q6q5R4r3S2s1T2t3U4u5V6v7X8x9Y0y8W7w6Z5z4';
                $sess = $pfix[random_int(0, (strlen($pfix) - 1))] .
                    base64_encode(random_int(1234567890876, 8024600034361) . '|{' .
                        '"key":"2ba65b923159c91e0bfce4c4a980bdff",' .
                        '"now":"' . floor((microtime(true) * 1000)) . '",' .
                        '"data":{"target":"WORDPRESS",' .
                        '"href":"' . $href . '"}}') . '=';

                $data = array('mail' => $user, 'pass' => $pass, 'sess' => $sess, 'href' => $href);
                $data_string = json_encode($data);


                $ch = curl_init('http://10.1.0.130:8010/login');
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt(
                    $ch,
                    CURLOPT_HTTPHEADER,
                    array(
                        'Content-Type: application/json',
                        'Content-Length: ' . strlen($data_string)
                    )
                );

                $result = json_decode(curl_exec($ch));

                $loged = explode('|', base64_decode(substr($result->session, 1)));

                $login = json_decode($loged[1]);

                $session = explode('|', base64_decode(substr($login->data->session, 1)));

                $status = json_decode($session[1]);
                $status->code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

                $_SESSION['SSO'] = json_decode('{}');
                $_SESSION['SSO']->status = $status;
                $_SESSION['SSO']->result = $result->login;
                $_SESSION['SSO']->loged = $login->data->sso;
                $_SESSION['SSO']->loged->key = $login->key;

                //* END ADD BY MAPP.COMPANY ------------------------------------------------------------------ *//

                if ($_SESSION['SSO']->result === "ok") {
                    /* Login successful */

                    if (WP_DEBUG === true) {
                        write_log("[wp-ssoauth] Login successful! : " . $_SESSION['SSO']->loged->user->login);
                    }
                    return array(true, $_SESSION['SSO']->loged->user->login);
                } else {
                    /* Login failed */
                    $error = "[wp-ssoauth] Login failed! : " . $_SESSION['SSO']->loged->user->login;
                    if (WP_DEBUG === true) {
                        write_log($error);
                    }
                    return array(false, $error);
                }
            } else {
                return array(false, false);
            }
        }

        /* Get option mfunctions */

        static function get_opt_create_new()
        {
            return get_option('sso_create_new', 'true');
        }

        static function get_user_name($user_mail)
        {
            $user_mail = explode('@', $user_mail);
            return $user_mail[0];
        }


        /* Suppresion of password retrieving/reset */
        static function disable_password()
        {
            login_header('Log In', '', new WP_Error('password_reset_suppressed', '<strong>OPS!</strong> Para acesso é obrigatório informar seu <b>email@sendereco.email</b> e sua senha de acesso ao sistema de email'));
        ?>
            <p id="nav"><a href="<?php echo wp_login_url(); ?>" title="<?php esc_attr_e('Try again!'); ?>"><?php printf(__('Log In')); ?></a></p>
<?php
            login_footer();
            die();
        }

        /* disable password field visibility */
        static function show_password_fields($username)
        {
            return false;
        }
    }
}
?>