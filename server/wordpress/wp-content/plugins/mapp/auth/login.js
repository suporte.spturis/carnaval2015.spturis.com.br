/**
 * --------------------------------------------------
 * Copyright (c) 2020 - 2021 Alailson Barbosa Ribeiro
 * --------------------------------------------------
 * @ Author Info: ribeiro@alailson.com.br
 * @ Create Time: 2021-11-26 08:48:33
 * @ Modified At: 2021-11-29 05:13:54
 * --------------------------------------------------
 * Author Quote: I love do this ♡ Enlightened by God!
 * --------------------------------------------------
 **/

window.onload = () => {
    document.getElementById("loginform").style.display = "none";
    var e = document.createElement('div');
    e.id = "logo-prefeitura";
    e.style.width = '100%';
    e.style.textAlign = 'center';
    e.innerHTML = `<a href="http://capital.sp.gov.br" target="_blank" style="text-decoration: none;"><img src="https://img.spturis.com.br/prefeitura/logo.png" width="150px"></a>`;
    document.getElementById("login").appendChild(e);


    document.getElementById("nav").innerHTML = `
<div id="cloudflare-capatcha" style="border-width: 4px !important;border-color: coral;border-style: solid;border-radius: 5px;">
    <div>
        <img src="https://img.spturis.com.br/capatcha.png" width="100%">
    </div>
    <div style="padding: 10px;text-align: center;color: black;background-color: white;">
    Essa tela, é uma proteção extra contra sistemas robotizados e simultâneos de acesso indevido.<br />
    <img src="https://img.spturis.com.br/carregando-info.gif" width="100%">
        <strong>LEMBRE-SE:</strong><br />
        <b>NUNCA COMPARTILHE SUA SENHA!</b>
    </div>
    </div>`;

    setTimeout(() => {
        document.getElementById("user_login").disabled = false;
        document.getElementById("user_pass").disabled = false;
        document.getElementById("loginform").style.display = "block";
        document.getElementById("nav").innerHTML = ``;
    }, 5000);

}