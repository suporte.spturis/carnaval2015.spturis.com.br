<?php
// inclui o header
get_header();

//looping
while (have_posts()): the_post();

    // escolhe as cores
    include ('sidebar-escolheacor.php');
    ?>

    <div id="noticias" style="color: <?php echo $cor1; ?>;">

        <section id="noticias_section">



            <div id="noticias_section_head">
                <div id="noticias_section_head_data">
                    <?php the_date('l, F j, Y'); ?>
                </div>
                <div id="noticias_section_head_title">
                    <?php the_title(); ?>
                </div>

                <div id="noticias_section_head_relacionadas">
                    
                </div>

                <div id="noticias_section_head_social">

                </div>
            </div>
            
            
            <div id="noticias_section_conteudo">
                <?php the_content(); ?>
            </div>
            
            
            
            
            
            
            
            
            




            <!-- Leia Mais -->
            <?php
            include ('sidebar-leiamais.php');
            ?>
            <!-- Leia Mais -->
        </section>
        <!-- Aside -->
        <aside id="aside">
            <?php
            include ('sidebar-publicidade.php');
            include ('sidebar-maisnoticias.php');
            ?>
        </aside>
        <!-- Aside -->
    </div>
<?php endwhile; ?>
<?php get_footer(); ?>