/**
 * --------------------------------------------------
 * Copyright (c) 2020 - 2021 Alailson Barbosa Ribeiro
 * --------------------------------------------------
 * @ Author Info: ribeiro@alailson.com.br
 * @ Create Time: 2021-11-26 01:02:40
 * @ Modified At: 2021-11-26 01:40:53
 * --------------------------------------------------
 * Author Quote: I love do this ? Enlightened by God!
 * --------------------------------------------------
 **/
setTimeout(() => {
    var owner = {
        copyright: "São Paulo Turismo S/A. CNPJ: 62.002.886/0001-60",
        government: "Prefeitura Municipal da Cidade de São Paulo, SP, Brasil"
    }

    console.log(JSON.stringify(owner));

    var provider = document.createElement("meta");
    provider.setAttribute("name", "copyright");
    provider.content = owner.copyright;
    document.getElementsByTagName("head")[0].appendChild(provider);
    var provider = document.createElement("meta");
    provider.setAttribute("name", "government");
    provider.content = owner.government;
    document.getElementsByTagName("head")[0].appendChild(provider);

    var provider = document.createElement("meta");
    provider.setAttribute("name", "serviceprovider");
    provider.content = "https://mapp.company";
    document.getElementsByTagName("head")[0].appendChild(provider);
    var developer = document.createElement("meta");
    developer.setAttribute("name", "developer");
    developer.content = "https://alailson.com.br";
    document.getElementsByTagName("head")[0].appendChild(developer);
}, 3000);