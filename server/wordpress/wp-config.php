<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

/*

UPDATE wp_options SET option_value = replace(option_value, 'carnaval.spturis.com.br', 'carnaval2015.spturis.com.br');
UPDATE wp_posts SET guid = replace(guid, 'carnaval.spturis.com.br', 'carnaval2015.spturis.com.br');
UPDATE wp_posts SET post_content = replace(post_content, 'carnaval.spturis.com.br', 'carnaval2015.spturis.com.br');
UPDATE wp_postmeta SET meta_value = replace(meta_value,'carnaval.spturis.com.br', 'carnaval2015.spturis.com.br');

*/


define('WP_SITEURL','https://carnaval2015.spturis.com.br/');
define('WP_HOME','https://carnaval2015.spturis.com.br/');

if (strpos($_SERVER['HTTP_X_FORWARDED_PROTO'], 'https') !== false)
   $_SERVER['HTTPS'] = 'on';



// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'br.com.spturis.carnaval2015');

/** MySQL database username */
define('DB_USER', 'br.com.spturis.carnaval2015');

/** MySQL database password */
define('DB_PASSWORD', 'C1rn1v1l@2015@SPT4R3S');

/** MySQL hostname */
define('DB_HOST', 'mysql.database.spturis.com.br');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '0d8853b642cbc5f8e864e0a1b67fe1e5961d86ff');
define('SECURE_AUTH_KEY',  '5bac364478d1f98e40aca16900cb92caedf8316c');
define('LOGGED_IN_KEY',    '6d8dd94a1c47649ea78d736a33868cf8ac4bcc77');
define('NONCE_KEY',        'f7ed540a3e5a259aa451731c087159b04f8f0753');
define('AUTH_SALT',        '4af267ec7d875a290fdee638f82cdd896da563c9');
define('SECURE_AUTH_SALT', '6dccac5f18dd432512c9604e4ef94966649f7eab');
define('LOGGED_IN_SALT',   'e1b4e77019e91e8edbccd1599ace752d05e60c47');
define('NONCE_SALT',       '8ae2a2256124865f15886639c99efc357759acda');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', 'pt_BR');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
